﻿# program: extract_rubric_outcomes.py
# author:  Brent Neal Reeves
# version: 1.1
# copyright: GPL
# 
# notes: use canvas api to get rubric details for each course in an account
#
# usage: 
# python3 ./extract_rubric_outcomes.py
# python3 ./extract_rubric_outcomes.py --course 1025 --years 2122 
#
# 
# "-i", "--noinit",  help="initialize database",                    action="store_false")
# "-a", "--account", help="Canvas account number - default 28",     default='28')    
# "-y", "--years",   help="2 2-digit years, default 2122",          default='2122')    
# "-o", "--out",     help="csv output filename, default 'out.csv'", default='out.csv')
# "-v", "--verbose", help="verbosity level 0-4",                    default='0')
# "-m", "--max",     help="max # of courses, default 999999",       default='999999')
# "-p", "--prefix",  help="Prefixes, default PO|SLO|MBA|BUS|MKT|MGT", default='PO|SLO|MBA|BUS|MKT|MGT')
# "-c", "--course",  help="include just the given course, default ALL",    default="ALL")
# "-d", "--dump",    help="dump database to see the tables, default false",action="store_true")
#
# out:
#  CSV files
#
# in:
# codes.csv should look something like:
#
# Code,Description
# ACCT 1.1,"Students will be able to file itemized deductions."
# ACCT 4.3,Students will analyze a recent case of corporate fraud to analyze the actions of management.
# BBA 1.4,"Students will reflect upon personal motivations related to wealth, service, and giving."
#

import os
import time
import datetime
import requests
import sqlite3
import csv
import collections
import sys, argparse
import subprocess
import json
import re
from html.parser import HTMLParser
import config

URL_COURSE  = 'https://acuonline.instructure.com/api/v1/courses/'
URL_ACCOUNT = 'https://acuonline.instructure.com/api/v1/accounts/'

AUTH = config.tokenAccount
CODESFILE = 'codes.csv'

CONN = ''
DB = ''
LOGLEVEL = 0

def load_codes():
    log(1, "load_codes() called)")
    # DB.execute("drop table if exists codes")
    DB.execute('create table if not exists codes (id string, description string, primary key (id))')

    with open(CODESFILE, newline='') as csvfile:
        log(1, ("load_codes() opened [%s])" % CODESFILE))
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
        for row in spamreader:
            # print(', '.join(row))
            davals = (row[0], row[1])

            sql = "insert or ignore into codes ( id, description ) values (?,?) "
            DB.execute( sql, davals )

        CONN.commit()


def db_setup( noinit ):
    global DB, CONN
    log(1, ("db_setup noinit: [%s]" %  noinit))
    # :memory: for database in RAM
    CONN = sqlite3.connect('rubrics_mba.db')
    DB = CONN.cursor()

    if (not noinit):
        # DB.execute("drop table if exists sub")
        CONN.commit()

        DB.execute("drop table if exists asg")
        DB.execute("create table if not exists asg (id string, name string, muted bool, due_at date, "\
            "points_possible float, omit_from_final_grade bool, course_id string, accType string, "\
            "accN string, accBoth string, accTitle string, primary key(id))")

        DB.execute("drop table if exists rubric")
        DB.execute("create table if not exists rubric (id string, asg_id string, course_id string, points float, "\
            "description string, accType string, accN string, accBoth string, accTitle string, "\
            "primary key (id, asg_id))")

        DB.execute("drop table if exists score")
        DB.execute('create table if not exists score (id string, login string, name string, asg_id string, '\
            'course_id string, rubric_id string, points float, primary key (id, asg_id, rubric_id))')

        DB.execute("drop table if exists codes")
        DB.execute('create table if not exists codes (id string, description string, primary key (id))')

        # DB.execute("drop table if exists keepers")
        DB.execute(
            'create table if not exists keepers ( '\
            'courseId string, coursename string, coursekey string, '\
            'year1 integer, year2 integer, semester string, semester_number string, grad string, '\
            'login string, student string, asgId string, accType string, accN string, asgTag string, '\
            'asgName string, due date, asgPossibles float, rubricId string, rubricPoints float, points float, '\
            'pcnt float, rubricTag string, description string, parent string)'
            )

        CONN.commit()

        
def db_dump():
    global DB

    sql = "select name from sqlite_schema where type = 'table' and name not like 'sqlite_%'"
    for tab in DB.execute("select name from sqlite_schema where type = 'table' and name not like 'sqlite_%'"):
        # "select 'Count of %s', count(*) from %s" % (t, t)):
        print("\ntab: %s" % tab)
        # for row in DB.execute("select 'Count of %s', count(*) from %s" % (tab, tab)):
        #     print("\nTABLE: %s" % tab)
        #     print(row)

    # for t in ['asg',  'rubric', 'score']:
    #     for row in DB.execute("select 'Count of %s', count(*) from %s" % (t, t)):
    #         print("\nTABLE: %s" % t)
    #         print(row)

    # for t in ['asg', 'rubric', 'score']:
    #     print("\nTABLE: %s" % t)
    #     for row in DB.execute("select * from %s order by id" % (t)):
    #         print(row)

            
def log (level, str):
    global LOGLEVEL
    if (level < LOGLEVEL):
        print (str)


def get_courses( in_url, in_account ):
    global AUTH
    log(1, "get_courses")

    h = {'Authorization': AUTH}
    uri = in_url + in_account + '/courses' + '?per_page=99'

    r = requests.get(uri, headers= h)
    log(2, 'uri: %s' % str(uri))
    log(2, 'request: %s' % str(r))

    data_set = []
    rj = r.json()

    for d in rj:
        data_set.append(d)

    # caution 'last' might not be in the list, so immediate-if it
    keepon = ('last' not in r.links) or (r.links['current']['url'] != r.links['last']['url'])
    while keepon:
        # print("\nnext links: %s" % str(r.links))
        r = requests.get(r.links['next']['url'], headers= h)  
        rj = r.json()  
        for d in rj:  
            data_set.append(d)
        keepon = ('last' not in r.links) or (r.links['current']['url'] != r.links['last']['url'])

    log(2, "Account: %s Courses: %d" % (in_account, len(data_set)))
    with open('mba_courses.json', 'w') as f:
        json.dump(data_set, f)

    return data_set


def process_courses_of_account( urlAccount, urlCourse, account, onecourse, year_pattern, prefix, max=99999  ):
    """
    Parameters
    urlAccount : str
    urlCourse : str
    account : int
    onecourse: course id
    year_pattern: str comma-separated list of 2 digit years, e.g. '18,19'
    prefix: PO|SLO|MBA|BUS|MKT|MGT
    max: int bail after X items, for debugging
??
"""
    global URL
    log(1, "process_courses_of_account max: %s acct: %s  course: %s" % (max, account, onecourse))

    # todo:
    #  if course given, just get that course instead of fetching whole list
    #
    courses = get_courses( urlAccount, account )
    if ( int(max) < 0):
        print ( "process_courses_of_account < 0" )
        i=0
        for c in courses:
            print( "%05d %s" %  ( int(c['id']), c['name']))
            i = i+1
        return

    i = 0
    log(2, "Account %s has %d courses start" % (account, len(courses)))

    kept = 0;
    for c in courses:
        cid = str( c['id'] )
        cshort = str( c['course_code']+'-'+ cid )
        n = cshort.find("-")
        if (n < 0):
            ff = len(cshort)
        else:
            ff = len(cshort) - n - 1
        coursenum = cshort[-ff:].strip()
        coursenum = coursenum.replace(" ","_")

        log(2, "   course: %s   id: %s   short: %s   num: %s" % (str(c), cid, cshort, coursenum ))
        i = i + 1
        if (i > max):
            break

        cis_course_id = str( c['sis_course_id'] )
        cisyears = cis_course_id[0:4] # strip '1819' off of front of '1819SU1G_BUSA636'
        keepers = year_pattern.split(',') 
        keep_this_one = False
        for yy in keepers:
            if (cisyears == yy):
                log(3, 'keeping %s because %s of %s' % (cisyears, yy, keepers) )
                keep_this_one = True
                break
            else:
                log(3, 'skipping %s because %s' % (cisyears, yy) )
                
        # print(onecourse + "? " + cid + " -- " + cshort)
# 2021-08-05 13:16:38 Begin
# Processing 2021FA1G_BUSA550 course BUSA 550-3011
# Kept 134 and skipped 433 courses.
# 2021-08-05 13:18:41 End
            
        if (keep_this_one):
            kept += 1
            if ((onecourse == "ALL") | (cid == onecourse)):
                print( 'Processing %s-%s course %s' % (cis_course_id, coursenum, cshort ) )

                setup(False)
                get_scores( urlCourse, cid )
                load_scores( cid )
                
                get_assignments( urlCourse, cid )
                load_assignments(prefix)
                
                # write_course_csv( cshort.replace(' ','_') )
                write_course_csv( cis_course_id.strip() + "-" + coursenum)
            else:
                log(2,
                    "  refusing to process onecourse: %s  cid: %s  cshort: %s \ncourse: %s\n" %
                    (onecourse, cid, cshort, c))
            
    log(2, "Account %s has %d courses end" % (account, len(courses)))
    print ("Kept %d and skipped %d courses." % (kept, (len(courses) - kept)))
 

def get_assignments(in_url, in_course):
    global AUTH
    log(1, "get_assignments")

    h = {'Authorization': AUTH}
    uri = in_url + in_course + '/assignments/' + '?style=full&per_page=99'

    r = requests.get(uri, headers= h)
    log(3, 'uri: %s' % str(uri))
    log(3, 'request: %s' % str(r))

    data_set = []
    rj = r.json()
    for d in rj:
        data_set.append(d)

    keepon = ('last' not in r.links) or (r.links['current']['url'] != r.links['last']['url'])
    while keepon:
        r = requests.get(r.links['next']['url'], headers= h)  
        rj = r.json()  
        for d in rj:  
            data_set.append(d)
        keepon = ('last' not in r.links) or (r.links['current']['url'] != r.links['last']['url'])

    log(2, "Course %s  assignments count: %d" % (in_course, len(data_set)))
    with open('mba_assignments.json', 'w') as f:
        json.dump(data_set, f)

def clean(text):
    rc = re.sub(r'[^\x00-\x7f]',r'', text)
    # print(f'clean: {text}')     
    if ( "-" == rc[:1] ):
        rc = rc[1:]
    rc = rc.strip()
    return rc
    

def load_assignments(prefix):
    global DB, CONN
    log(1, 'load_assignments')
    with open('mba_assignments.json', 'r') as f:
        try: 
            j = json.load( f )
            fields = ['id', 'name', 'muted', 'due_at', 'points_possible', 'omit_from_final_grade', 'course_id', 'accType', 'accN', 'accBoth', 'accTitle']
            fs = ''
            names = ', '.join(fields)
            aPattern = '(' + prefix + ')[_ ]([a-zA-Z0-9.]*) (.*)'
            log(1, 'pattern: ' + aPattern)
    #        pattern = re.compile('(PO|SLO|MBA|BUS|MKT|MGT)[_ ]([a-zA-Z0-9.]*) (.*)')
            pattern = re.compile( aPattern )

            for r in j:
                accType = ''
                accN = ''
                accBoth = ''
                accTitle = ''
                m = pattern.match( r["name"] )
                if m:
                    accType = m.group(1)
                    accN = m.group(2)
                    accBoth = accType + " " + accN
                    accTitle = m.group(3)

                sql = "insert into asg( " + names + ") values (?,?,?,?,?,?,?,?,?,?,?) "

                try:
                    trimmedName = clean( r["name"] )
                    trimmedTitle = clean( accTitle )
                    
                    non_null_id  = r["id"]

                    davals = ( non_null_id, trimmedName, r["muted"], r["due_at"], r["points_possible"], r["omit_from_final_grade"], r["course_id"], accType, accN, accBoth, trimmedTitle )
                    course_id = r["course_id"]
                    asg_id = non_null_id

                    DB.execute( sql, davals )
                except (TypeError) as e:
                    print("\n  Oops load_assignments TypeError: {0}".format(e))
                    print("  Exception: %s" , ( ' '.join( map(str, davals) )))
                except (KeyError) as e:
                    print("\n  Oops load_assignments KeyError: {0}".format(e))
                    print("  Exception: %s" , ( ' '.join( map(str, davals) )))
                    print( str(davals) )
                except:
                    print("\n  Oops load_assignments Exception: ", sys.exc_info()[0])
                    print("  Exception: %s" % ( ' '.join( map(str, davals) )))
                    print("  Exception: %s" % ( str(davals) ))
                    print("  Exception: sql %s" % sql)

                # get the rubrics, if exist
                if "rubric" in r:
                    for rr in r["rubric"]:
                        accType = ''
                        accN = ''
                        accBoth = ''
                        accTitle = ''
                        m = pattern.match( rr["description"] )
                        if m:
                            accType = m.group(1)
                            accN = m.group(2)
                            accBoth = accType + " " + accN
                            accTitle = m.group(3)
                        # rubric_id, assignment, course, points, description, accType, accN, accBoth, accTitle
                        trimmedTitle = clean( accTitle )
                        sql = ("insert or ignore into rubric (id, asg_id, course_id, points, description, accType, accN, accBoth, accTitle) values (?,?,?,?,?,?,?,?,?)")
                        nnid = rr["id"]
                        davals = (  nnid, asg_id, course_id, rr["points"], rr["description"],
                                    accType, accN, accBoth, trimmedTitle)
                        DB.execute( sql, davals )
        except (json.JSONDecodeError) as e:
            print("\n  Oops load_assignments JSONDecodeError {0}".format(e))
            print("  skipping %s" % prefix)
        except (ValueError) as e:
            print("\n  Oops load_assignments ValueError {0}".format(e))
            print("  skipping %s" % prefix)

        CONN.commit()
        

def get_scores(in_url, in_course):
    global AUTH
    log(1,"get_scores course: %s  url: %s" % (in_url, in_course))

    h = {'Authorization': AUTH}

    data_set = []
    uri = in_url + in_course + '/students/submissions?style=full&per_page=99&student_ids[]=all&include[]=rubric_assessment&include[]=user'
    r = requests.get(uri, headers= h)
    log(3, 'uri: %s' % str(uri))
    log(3, 'request: %s' % str(r))

    raw = r.json()
    for d in raw:
        data_set.append(d)

    log(3, 'r.links %s' % str(r.links))

    keepon = ('last' not in r.links) or (r.links['current']['url'] != r.links['last']['url'])
    while keepon:
        r = requests.get(r.links['next']['url'], headers= h)  
        raw = r.json()  
        for question in raw:  
            data_set.append(question)
        keepon = ('last' not in r.links) or (r.links['current']['url'] != r.links['last']['url'])

    log(2, "Course: %s  scores count: %d" % (in_course, len(data_set)))
    with open('mba_scores.json', 'w') as f:
        json.dump(data_set, f)

# login and rubricId still null
# course,coursename,year1,year2,semester,semester_number,grad,login,student,asgId,accType,accN,asgTag,asgName,due,asgPossibles,rubricId,rubricPoints,points,pcnt,rubricTag,description
# 49,BUSA550,1617FA2G_BUSA550-550,16,17,FA,2,G,,,874,,,,,2016-10-19T04:59:00Z,1.0,,,,,,
# 49,BUSA550,1617FA2G_BUSA550-550,16,17,FA,2,G,,,875,,,,,2016-10-21T04:59:00Z,1.0,,,,,,
# 49,BUSA550,1617FA2G_BUSA550-550,16,17,FA,2,G,,,876,,,,,2016-10-26T04:59:00Z,1.0,,,,,,
# 49,BUSA550,1617FA2G_BUSA550-550,16,17,FA,2,G,,,877,,,,,2016-10-28T04:59:00Z,1.0,,,,,,
# 49,BUSA550,1617FA2G_BUSA550-550,16,17,FA,2,G,,,878,,,,,2016-11-04T04:59:00Z,1.0,,,,,,

def load_scores(in_course):
    global CONN
    log(1,"load_scores %s" % in_course)

    with open('mba_scores.json', 'r') as f:
        try:
            j = json.load( f )
            for r in j:
                # print("r: %s" % r)
                user_id = r["user_id"]
                asg_id = r["assignment_id"]

                # user_id = r["user_id"]

                # scores (id string, asg_id string, course_id string, rubric_id string, points float, primary key (id, asg_id, rubric_id))

                try:
                    name = r["user"]["sortable_name"]
                    login = r["user"]["id"]
                    # login = r["user"]["login_id"] acu instance "bnr01a"

                    sql = ("insert into score (id, login, name, asg_id, course_id, rubric_id, points) values (?,?,?,?,?,?,?)")
                    davals = (  user_id, login, name, asg_id, in_course, '', r["entered_score"])
                    DB.execute( sql, davals )
                    CONN.commit()

                    if ( "rubric_assessment" in r ):
                        for k,v in r["rubric_assessment"].items():
                            sql = ("insert  into score (id, login, name, asg_id, course_id, rubric_id, points) values (?,?,?,?,?,?,?)")
                            if "points" in v:
                                vp = v["points"]
                            else:
                                vp = ''
                            davals = ( user_id, login, name, asg_id, in_course, k, vp)
                            DB.execute( sql, davals )
                            CONN.commit()

                except (KeyError) as e:
                    print("\n  Oops KeyError course %s " % in_course)
                    print("  Oops KeyError: {0}".format(e))
                    print("  Oops Course: %s ScoreCount: %d " % (in_course, len(j)))
                    print("  Oops json: %s " % json.dumps(r))
                except:
                    e = sys.exc_info()[0]
                    print("\n  Oops KeyError course %s " % in_course)
                    print("  Oops Course: SomeError: {0}".format(e))
                    print("  Oops Course: %s  ScoreCount %d json: %s " % (in_course, len(j), json.dumps(r)))
        except (json.JSONDecodeError) as e:
            print("\n  Oops load_scores JSONDecodeError {0}".format(e))
            print("  skipping %s" % in_course)
        except (ValueError) as e:
            print("\n  Oops load_scores ValueError {0}".format(e))
            print("  skipping %s" % in_course)

        CONN.commit()


def setup(noinit):
    db_setup(noinit)
    load_codes()

    if (LOGLEVEL > 3):
        db_dump()

    
def shutdown():
    CONN.close()


def write_course_csv( course ):
    log(2, 'write_course_csv: %s ' % course)
    # write_csv( "mba-%s.csv" % course )
    write_csv( course )


def write_csv(coursename = 'mba-default-filename'):
    global DB, CONN
    log(2, 'write_csv: %s ' % coursename)
    filename = ("%s.csv" % coursename)
    n1 = coursename.find("_") + 1
    n2 = coursename.find("-",n1)
    cn = coursename[n1:n2]

    sql = 'select a.course_id courseId, '\
        '"' + cn +              '" coursename, '\
        '"' + coursename +      '" coursekey, '\
        '"' + coursename[0:2] + '" year1, '\
        '"' + coursename[2:4] + '" year2, '\
        '"' + coursename[4:6] + '" semester, '\
        '"' + coursename[6:7] + '" semester_number, '\
        '"' + coursename[7:8] + '" grad, '\
        's.login as login, '\
        's.name as student, '\
        'a.id as asgId, '\
        'a.accType, a.accN, a.accBoth asgTag, a.accTitle asgName, a.due_at due, a.points_possible asgPossibles, '\
        'r.id rubricId, '\
        'CASE WHEN r.points IS NULL THEN 0 ELSE r.points END as rubricPoints, '\
        'CASE WHEN s.points IS NULL THEN 0 ELSE s.points END as points, (s.points / r.points) pcnt, '\
        'r.accBoth rubricTag, r.accTitle description, cc.description parent '\
        'from asg a left outer join rubric r on (r.asg_id = a.id) '\
        'left outer join score s on (s.asg_id = a.id and s.rubric_id = r.id) '\
        'left outer join codes cc on (r.accBoth = cc.id) '\
        'order by coursename, a.id, s.name;'
        
    log(1, 'sql: ' + sql)
    log(1, '')
    # print('Yo')
    DB.execute(sql)

    # if (len(DB) > 0): unfortunately, SQLITE3 doesn't give the rowcount of a SELECT statement...
    
    rows = 0
    with open(filename, 'w') as f:
        writer = csv.writer(f)
        writer.writerow([d[0] for d in DB.description])
        for row in DB:
            writer.writerow(row)
            rows+= 1

    if (rows < 1):
        # delete the file we just created because it has no rows of data
        # there isn't a way for SQLITE3 to tell us how many rows are in a SELECT query...
        os.remove (filename)
        log(1, 'write_csv deleted empty file: %s' % coursename)
        print('write_csv deleted empty file: %s' % coursename)
    else:
        sql2 = 'insert into keepers (courseId, coursename, coursekey, year1, year2, semester, semester_number, grad, '\
            'login, student, asgId, accType, accN, asgTag, asgName, due, asgPossibles, rubricId, rubricPoints, points, '\
            'pcnt, rubricTag, description, parent) ' + sql

        DB.execute(sql2) # beautiful
    
    CONN.commit()


def handle_args(argv):
    global LOGLEVEL
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--noinit",  help="initialize database",                    action="store_false")
    parser.add_argument("-a", "--account", help="Canvas account number - default 28",     default='28')    
    parser.add_argument("-y", "--years",   help="2 2-digit years, default 2122",          default='2122')    
    parser.add_argument("-o", "--out",     help="csv output filename, default 'out.csv'", default='out.csv')
    parser.add_argument("-v", "--verbose", help="verbosity level 0-4",                    default='0')
    parser.add_argument("-m", "--max",     help="max # of courses, default 999999",       default='999999')
    parser.add_argument("-p", "--prefix",  help="Prefixes, default PO|SLO|MBA|BUS|MKT|MGT", default='PO|SLO|MBA|BUS|MKT|MGT')
    parser.add_argument("-c", "--course",  help="include just the given course, default ALL",    default="ALL")
    parser.add_argument("-d", "--dump",    help="dump database to see the tables, default false",action="store_true")

    args = parser.parse_args()
    l = int(args.verbose)

    if ( l > 0 ):
        print( "handle_args verbose %s" % str(l) )
        LOGLEVEL = l
        print ("Args %s" % str(args))

    Args = collections.namedtuple('Args', 'noinit, account, out, verbose, max, dump, course, prefix, years')
    return( Args(args.noinit, args.account, args.out, args.verbose, args.max, args.dump, args.course, args.prefix, args.years) )


def printNow(title):
    ts = time.time()
    st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
    print (st + ' ' + title)
    return (st + ' ' + title)


#---------------------------------------------------------------------------------
# main
#---------------------------------------------------------------------------------
def main(argv):
    beginning = printNow("Begin")
    args = handle_args(argv)

    log(2, str(args))

    setup(args.noinit)
    
    process_courses_of_account( URL_ACCOUNT, URL_COURSE,
                                args.account,
                                args.course,
                                args.years,
                                args.prefix,
                                int(args.max)
                                 )

    printNow("End")
    shutdown()

if __name__ == "__main__":
   main(sys.argv[1:])
