# Extracting Rubrics from Canvas

What if you could automate accreditation reports? That would be pretty great.

Follow these steps:

1. create a file named codes.csv to document your codes. Something like:
```Code,Description
ACCT 1.0,Students will be able to prepare reports appropriate to various areas in accounting.
ACCT 1.1,"Students will be able to complete an individual tax return with itemized deductions, capital gains, and supplemental income."
ACCT 1.2,Students will be able to construct financial statements from a trial balance.
ACCT 1.3,Students will be able to construct a set of consolidated financial statements from subsidiary company data.
BBA 1.4,"Students will reflect upon personal motivations and experiences related to wealth, service, and giving."
BBA 2.0,"Students will demonstrate broad, integrated knowledge and skills in core business disciplines and the business context."
etc.
```
2. prefix the names of the rubrics, for example, change "Write a paper on tyrannical governments." to "SLO 2.1 Write a paper on tyrannical governments."
3. use this python script to extract the detailed level aspects of those rubric assignments
4. use Excel filter and pivot table to count all the things

# Running this script

```
python3 extract_rubric_outcomes.py
```

...often produces output that looks something like:
```
❯ python3 ./extract_rubric_outcomes.py --years 2122
2022-06-03 16:40:43 Begin
Processing 2122FA2G_CSO116-4500 course CSO 116-4500
Processing 2122FA2G_ITO325-4503 course ITO 325-4503
write_csv deleted empty file: 2122FA2G_ITO325-4503
Processing 2122FA2G_ITA491-4507 course ITA 491-4507
Processing 2122FA2G_ITO310-4509 course ITO 310-4509
Processing 2122FA2G_ITO220-4513 course ITO 220-4513
Processing 2122FA2G_ITA475-4515 course ITA 475-4515
Processing 2122FA2G_ITA110-4527 course ITA 110-4527
Processing 2122FA2G_ITO221-4529 course ITO 221-4529
Processing 2122FA2G_ITA405-4532 course ITA 405-4532
Processing 2122FA2G_PLCO365-4539 course PLCO 365-4539
write_csv deleted empty file: 2122FA2G_PLCO365-4539
Processing 2122FA2G_PLSO432-4540 course PLSO 432-4540
Processing 2122FA2G_MKTO342-4548 course MKTO 342-4548
...

```
and will produce a bunch of CSV files like so:
```
2122FA2G_CSO116-4500.csv
2122FA2G_ITA110-4527.csv
2122FA2G_ITA405-4532.csv
2122FA2G_ITA475-4515.csv
2122FA2G_ITA491-4507.csv
2122FA2G_ITO220-4513.csv
2122FA2G_ITO221-4529.csv
2122FA2G_ITO310-4509.csv
2122FA2G_PLSO432-4540.csv
2122SU1G_BUSA674-5716.csv
```

Each of those files will look something like:
(student ids below have been replaced with "s1" "s2"...)

```
course,login,name,asgId,accType,accN,AssignTag,asgName,due,asgPossibles,rubricId,rubricPoints,points,pcnt,RubricTag,Description
1318,3103,"s10",27208,MBA,2.1,MBA 2.1,Market Prices,2018-10-25T04:59:00Z,2.0,_1837,0.1,0.1,1.0,MBA 3.2,"The post meets basic writing standards, including grammar, usage, spelling, punctuation, and organization."
1318,3103,"s10",27208,MBA,2.1,MBA 2.1,Market Prices,2018-10-25T04:59:00Z,2.0,_4900,0.6,0.6,1.0,MBA 3.1,How have these behaviors influenced the changes in gas prices?
1318,3103,"s10",27208,MBA,2.1,MBA 2.1,Market Prices,2018-10-25T04:59:00Z,2.0,_6906,0.1,0.05,0.5,MBA 3.2,"The post is formatted based on APA guidelines, and includes in-text citations and a reference page when cited?"
1318,3103,"s10",27208,MBA,2.1,MBA 2.1,Market Prices,2018-10-25T04:59:00Z,2.0,_7703,0.6,0.6,1.0,MBA 2.1,How have gas prices changed throughout this past year?
1318,3103,"s10",27208,MBA,2.1,MBA 2.1,Market Prices,2018-10-25T04:59:00Z,2.0,_9367,0.6,0.36,0.6,MBA 2.1,How have the supply and demand of gas been influenced by our behavior and the behavior of corporations?
1318,2571,"s8",27208,MBA,2.1,MBA 2.1,Market Prices,2018-10-25T04:59:00Z,2.0,_1837,0.1,0.1,1.0,MBA 3.2,"The post meets basic writing standards, including grammar, usage, spelling, punctuation, and organization."
1318,2571,"s8",27208,MBA,2.1,MBA 2.1,Market Prices,2018-10-25T04:59:00Z,2.0,_4900,0.6,0.6,1.0,MBA 3.1,How have these behaviors influenced the changes in gas prices?
1318,2571,"s8",27208,MBA,2.1,MBA 2.1,Market Prices,2018-10-25T04:59:00Z,2.0,_6906,0.1,0.0,0.0,MBA 3.2,"The post is formatted based on APA guidelines, and includes in-text citations and a reference page when cited?"
1318,2571,"s8",27208,MBA,2.1,MBA 2.1,Market Prices,2018-10-25T04:59:00Z,2.0,_7703,0.6,0.6,1.0,MBA 2.1,How have gas prices changed throughout this past year?
1318,2571,"s8",27208,MBA,2.1,MBA 2.1,Market Prices,2018-10-25T04:59:00Z,2.0,_9367,0.6,0.6,1.0,MBA 2.1,How have the supply and demand of gas been influenced by our behavior and the behavior of corporations?
```


# Details

## Limit output to a given course

Running "python extract_rubric_outcomes.py" will download all of the files in that Canvas account.  
To limit the output to one course, use the --course argument

```
python3 extract_rubric_outcomes.py --course COURSE-ID-HERE
```

```
Here are some other command line arguments:
# 
# "-i", "--noinit",  help="initialize database",                    action="store_false")
# "-a", "--account", help="Canvas account number - default 28",     default='28')    
# "-y", "--years",   help="2 2-digit years, default 2122",          default='2122')    
# "-o", "--out",     help="csv output filename, default 'out.csv'", default='out.csv')
# "-v", "--verbose", help="verbosity level 0-4",                    default='0')
# "-m", "--max",     help="max # of courses, default 999999",       default='999999')
# "-p", "--prefix",  help="Prefixes, default PO|SLO|MBA|BUS|MKT|MGT", default='PO|SLO|MBA|BUS|MKT|MGT')
# "-c", "--course",  help="include just the given course, default ALL",    default="ALL")
# "-d", "--dump",    help="dump database to see the tables, default false",action="store_true")
```

## Canvas Auth Tokens

Since this uses the Canvas API, you'll need credentials.  This script
needs a variable named "tokenAccount" stored in a file named
config.py.  This repo comes with a file named
"example-rename-config.py" with a fake Bearer token.

The file example-rename-config.py contains:
```
tokenAccount  = 'Bearer  1~place-your-super-secret-and-very-long-key-here'
```


You'll need to update it with your secret key and rename it to
config.py.  

(If you're new to interacting with public repositories like this, you
might wonder why the file isn't just named "config.py" - ready for you
to edit.  The short answer is that when you have various folks
collaborating on repositories that involve secret tokens, we need a
way to share all the other stuff besides the tokens, and yet we want
each of us to be able to run the code.  So we store secrets in files
that are not uploaded to the repository.  Each of us has our own
config.py file with our own key.)

## Checking your Canvas tokens

To test your tokens, try something like this: (after replacing the url and key parts)

```
curl -g https://blee.instructure.com/api/v1/courses/a-course/discussion_topics/a-topic/entries -H 'Authorization: Bearer 1~place-your-super-secret-and-very-long-key-here' -D 'nextlink.txt' > happiness.txt
```

This will give you quick reality check - testing to see if your url
and key is even happy.


## sqlite notes

Notice the "extract_rubric_outcomes.py" script will leave data sitting
in a sqlite database, so if you're an SQL person, you can check it
out. Just look for a ".db" file in the run folder.

Then you can run sqlite3 itself and issue queries.  sqlite3 is not part of this repo, so you'll need to install that separately.

The following query opens up a "database" named rubrics_mba-1819.db
located in the current folder and runs a "select all" query against
the keepers table, sending the output to a text file named
nbes1819.csv.

> sqlite3 -header -csv rubrics_mba_1819.db "select \* from keepers;" > nbes1819.csv

The following commands, entered after starting sqlite3, run a query
and send output to nbes1819.csv.

```
> sqlite3 rubrics_mba_1819.db
> sqlite> .headers on
> sqlite> .mode csv
> sqlite> .output nbes1819.csv
> sqlite> SELECT customerid,
> ...> firstname,
> ...> lastname,
> ...> company
> ...> FROM customers;
> sqlite> .quit
```

Always Null? accType,accN,asgTag,asgName

Another sqlite3 command of interest is "COPY":

COPY persons(first_name, last_name, dob, email)
FROM 'C:\sampledb\persons.csv'
DELIMITER ','
CSV HEADER;
